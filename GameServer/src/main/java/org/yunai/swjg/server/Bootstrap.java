package org.yunai.swjg.server;

import org.slf4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.yunai.swjg.server.core.GameServerRuntime;
import org.yunai.swjg.server.core.GameServerShutdownHooker;
import org.yunai.swjg.server.core.ScannerThread;
import org.yunai.swjg.server.module.activity.template.ActivityTemplate;
import org.yunai.swjg.server.module.idSequence.IdSequenceHolder;
import org.yunai.swjg.server.module.item.template.ItemTemplate;
import org.yunai.swjg.server.module.monster.template.MonsterTemplate;
import org.yunai.swjg.server.module.monster.template.VisibleMonsterTemplate;
import org.yunai.swjg.server.module.partner.template.PartnerTemplate;
import org.yunai.swjg.server.module.player.template.LevelTemplate;
import org.yunai.swjg.server.module.player.template.ReputationTemplate;
import org.yunai.swjg.server.module.player.template.VocationTemplate;
import org.yunai.swjg.server.module.quest.template.QuestTemplate;
import org.yunai.swjg.server.module.rep.template.RepTemplate;
import org.yunai.swjg.server.module.scene.core.template.SceneTemplate;
import org.yunai.swjg.server.module.skill.template.SkillImpactTemplate;
import org.yunai.swjg.server.module.skill.template.SkillTemplate;
import org.yunai.yfserver.common.LoggerFactory;
import org.yunai.yfserver.server.ShutdownHooker;
import org.yunai.yfserver.util.MemoryUtils;

/**
 * 启动程序
 * User: yunai
 * Date: 13-3-16
 * Time: 下午5:49
 */
public class Bootstrap {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerFactory.Logger.server, Bootstrap.class);

    private static void init() {
        // 初始化Csv配置
        loadCSVs();

        // 初始化配置
        new ClassPathXmlApplicationContext("spring/applicationContext.xml");

        //

        // 初始化ShutdownHook
        initShutdownHook();

        // 设置服务器状态为开启
        GameServerRuntime.setOpenOn();
    }

    private static void initShutdownHook() {
        final ShutdownHooker shutdownHooker = new ShutdownHooker();
        shutdownHooker.add(new GameServerShutdownHooker());
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    // TODO ServerStatusLog.getDefaultLog().logStoppping();
                    shutdownHooker.run();
                } finally {
                    // TODO ServerStatusLog.getDefaultLog().logStopped();
                    System.out.println("媳妇是小呆瓜");
                }
            }
        });
    }

    private static void loadCSVs() {
        LOGGER.info("[loadCSVs] [load csv begin].");
        LevelTemplate.load();
        ReputationTemplate.load();
        PartnerTemplate.load(); // 依赖ReputationTemplate
        ItemTemplate.load();
        MonsterTemplate.load();
        SceneTemplate.load();
        RepTemplate.load();
        VisibleMonsterTemplate.load();
        ActivityTemplate.load();
        QuestTemplate.load();
        SkillImpactTemplate.load();
        SkillTemplate.load();
        VocationTemplate.load();
        LOGGER.info("[loadCSVs] [load csv success].");
    }

    public static void main(String[] args) {
//        LogFactory.useStdOutLogging(); // TODO 测试，将MYBATIS SQL 输出到控制台

        LOGGER.info("[main] [Game Server Start].");
        LOGGER.info("[main] [free:{} MB] [total:{} MB].", MemoryUtils.freeMemoryMB(), MemoryUtils.maxMemoryMB());

        init();

        IdSequenceHolder.init();

        LOGGER.info("[main] [Game Server Start success].");
        LOGGER.info("[main] [free:{} MB] [total:{} MB].", MemoryUtils.freeMemoryMB(), MemoryUtils.maxMemoryMB());

        new ScannerThread().start();
    }
}
